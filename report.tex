\documentclass{article}

\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{verbatim}
\usepackage{caption}
\usepackage{subcaption}

\usepackage[pdftex, pdftitle={Rapport projet}, pdfsubject={Rapport du simulateur de projet de système numérique}, colorlinks=true, linkcolor=black]{hyperref}

\title{
  Simulatotor: un simulateur de netlist \\
  \large -- projet de simulation numérique --}

\author{Maxime FLIN}

\begin{document}
\maketitle

\begin{abstract}
  Ce rapport donne la spécification du simulateur de \texttt{netlist} que j'ai implémenté pour le projet de systèmes numériques. J'ai de plus implémenté un compilateur (section \ref{sec:compile}) de \texttt{netlist} afin d'obtenir de meilleurs perfomances.
\end{abstract}

\section{Manuel d'utilisation}
\subsection{Organisation du dépôt}
Le projet a été réalisé en ocaml à partir de la base de code fourni pendant le TP1. Tous les fichiers ocaml du projet se trouvent directement à la racine. On trouve de plus un dossier \texttt{test} qui contient les fichiers \texttt{netlist} du premier TP et quelques fichiers de tests supplémentaires.

Les principaux fichiers du projet sont

\begin{description}
\item[main] Fichier d'entrée du projet
\item[io] Quelques variables globales\footnote{les paramètres passés par la ligne de commande} et fonctions d'utilitée générale comme la lecture d'un fichier rom ou de l'entrée standard
\item[ast, parser et lexer] Arbre de syntaxe abstraite, parseur et lexer ; essentiellement les fichiers du TP1.
\item[simulator] Simulation de netlist
\item[compiler] Compilation de netlist
\end{description}

\subsection{Compiler simulatotor}

Pour compiler le projet, j'ai fait le choix d'utiliser \texttt{dune}. Le plus simple est donc de l'installer si ce n'est pas déjà fait et de l'utiliser. Un fichier \texttt{Makefile} est fourni dans le projet mais ne fait rien d'autre que d'appeller \texttt{dune}. Pour compiler vous pouvez donc

\begin{itemize}
\item utiliser directement \texttt{dune} avec la commande \texttt{dune build main.exe}

\item utiliser \texttt{make} avec la commande \texttt{make all}
\item compiler le projet à la main (ce qui ne devrait pas être trop dur mais pourquoi se donner cette peine).
\end{itemize}

\subsection{Simuler avec simulatotor}

L'exécutable construit est assez simple à utiliser. Pour interpréter un fichier \texttt{netlist}, il suffit d'indiquer le chemin relatif jusqu'à ce fichier. Par exemple, la ligne de commande suivante exécute le fichier de test \texttt{fulladder.net}.

\begin{verbatim}
main.exe test/fulladder.net
\end{verbatim}

Le programme peut aussi prendre des options en fonction des besoins.

\begin{description}
\item[-v] Mode verbeux. Permet en particulier d'afficher le contenu des rams et des roms au cours de l'exécution.
\item[-n] Nombre de cycles à exécuter.
\item[-r <file>] Permet de préciser le contenu des roms. Le format d'un fichier rom est détaillé en section \ref{sec:romfile}
\item[-c] Mode compilation. Génère un fichier exécutable équivalent à l'interpréteur lorsque la séquence d'entrée est valide. Voir section \ref{sec:compile}.
\item[-k] Créer un fichier \texttt{C} qui simule la netlist.
\item[-o <file>] Permet de préciser le nom de l'exécutable lorsque la netlist est compilée. Par défaut \texttt{a.out}. Cette option est ignorée lorsque le mode compilation n'est pas actif.
\end{description}

Ainsi, la commande

\begin{verbatim}
main.exe -r my_rom_file.rom -c -k -o processeur processeur.net
\end{verbatim}

compile le fichier \texttt{processeur.net} vers un exécutable \texttt{processeur}, en conservant le fichier C associé dans \texttt{processeur.c}. Il contient dans sa rom les données décrites dans le fichier \texttt{my\_rom\_file.rom}.

\section{Le simulateur}
Le simulateur commence par réorganiser les différentes équations du fichier \texttt{netlist} afin, d'une part, de s'assurer de l'absence définitions cycliques et, d'autre part, qu'on prenne bien les définitions dans l'ordre. C'est à dire pour une définition du type $x = f(x_1, x_2, \cdots)$, on connaisse bien la valeur définitive  de tous les $x_i$ avant.

Il exécute ensuite les équations dans cet ordre les unes à la suite des autres à chaque étape.

Au début de chaque cycle, il attends sur l'entrée standard la séquence des entrées de la \texttt{netlist} dans le même ordre que dans le fichier \texttt{netlist}. Il précise chaque fois le nombre de bit attendu :

\begin{verbatim}
nom_de_l_entree (nombre de bits de cette entree) ?
\end{verbatim}

Le simulateur attends une suite de \texttt{0} et de \texttt{1} sans espaces. Une petite subtilité : pour être cohérent avec le format de lecture du compilateur (section \ref{sec:compile}), lorsque plusieurs bits sont attendus, ils sont écrit en gros-boutistes\footnote{du bit de poids le plus fort au bit de poids le plus faible}.

Par exemple, le fichier test \texttt{test/select.net} qui prend un entier sur 4 bits en série et les ressorts en parallèle s'exéute comme sur la figure \ref{fig:select}.

\begin{figure}[h!]
  \centering
\begin{subfigure}{.5\textwidth}
\begin{verbatim}
INPUT x
OUTPUT b0, b1, b2, b3
VAR
    x : 4, b0, b1, b2, b3
IN
b0 = SELECT 0 x
b1 = SELECT 1 x
b2 = SELECT 2 x
b3 = SELECT 3 x
\end{verbatim}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
\begin{verbatim}
Step 1:
x (4)? 1101
=> b0 = 1
=> b1 = 0
=> b2 = 1
=> b3 = 1
\end{verbatim}
\subcaption{un exemple de cycle d'exécution}
\end{subfigure}
\caption{Exécution du simulateur sur le fichier \texttt{test/select.net}\label{fig:select}}
\end{figure}

À la fin de chaque cycle il affiche les sorties de la \texttt{netlist}.

\section{Le compilateur\label{sec:compile}}

Le compilateur a un comportement analogue au simulateur. \textbf{Les entrées doivent êtres données en hexadécimales -- dans tous les cas elles seront interprêtées comme telles.}

Pour compiler un fichier \texttt{netlist}, j'ai fait le choix de le compiler vers C plutôt que directement vers de l'assembleur, estimant les optimisations de \texttt{gcc} plus conséquentes que ce que j'aurais été capable de fournir.

Le simulateur produit un fichier C qui simule à sa place le comportement de la \texttt{nestlist} donnée en entrée. Quelques libertés on été prises par rapport au comportement du simulateur. En effet, contrairement au simulateur la netlist compilée ne vérfie jamais que l'entrée est valide. De plus, le programme compilé ne prends pas d'entiers de plus de 64 bits.

Afin d'être le plus efficace possible, le simulateur demande à \texttt{gcc} d'optimiser autant que possible\footnote{avec le flag d'optimisation \texttt{-O3}}. Ce qui est parfois un peu trop, par exemple le fichier de test \texttt{test/clock\_div.net} sur la figure \ref{fig:clock} exécute $10^{10}$ cycles de trois portes\footnote{en retirant tous les prints} en un peu moins de 7 secondes. En regardant toutefois l'assembleur généré (figure \ref{fig:asm}), on remarque que le xor s'est perdu.

\begin{figure}[h!]
  \centering
  \begin{subfigure}{.35\textwidth}
\begin{verbatim}
INPUT
OUTPUT o
VAR
  _l_2, c, o
IN
c = NOT _l_2
o = REG c
_l_2 = REG o
\end{verbatim}
\subcaption{\\fichier \texttt{test/clock\_div.net}\label{fig:clock}}
\end{subfigure}
\begin{subfigure}{.55\textwidth}
\begin{verbatim}
movzbl 0x3002(%rip),%ecx        # 4029 <o>
movzbl 0x2ffc(%rip),%edx        # 402a <c>
movabs $0x2540be400,%rax

jmp    1042 <main+0x22>
nopw   0x0(%rax,%rax,1)
mov    %esi,%ecx
test   %cl,%cl
mov    %edx,%esi
sete   %dl
sub    $0x1,%rax
jne    1040 <main+0x20>
mov    %sil,0x2fd3(%rip)        # 4029 <o>

mov    %cl,0x2fcd(%rip)        # 402b <_l_2>
mov    %dl,0x2fc6(%rip)        # 402a <c>
\end{verbatim}
\subcaption{code assembleur produit par \texttt{gcc}\label{fig:asm}}
\end{subfigure}
\caption{}
\end{figure}
\newpage

\section{Les fichiers roms\label{sec:romfile}}
Une \texttt{netlist} peut faire appel à une rom au cours de son exécution. Les roms contiennent de la mémoire accessible en lecture seulement. Pour en préciser le contenu, on donne au simulateur (avec l'option \texttt{-r}) un fichier rom dont le format est le suivant:

\begin{figure}[h!]
  \centering
  \begin{subfigure}{.5\textwidth}
\begin{verbatim}
rom <addr_size> <word_size>
addr_1=data_1
addr_2=data_2
...
addr_n=data_n
;

rom ...
\end{verbatim}
  \end{subfigure}
  \caption{\texttt{addr\_size} et \texttt{word\_size} sont des entiers écris en décimal. \texttt{addr\_i} et \texttt{data\_i} sont des suites de \texttt{0} et \texttt{1} au même format qu'une entrée classique.}
\end{figure}

Une \texttt{netlist} compilé avec un fichier rom contient cette dernière en dur. Pour en changer, il faut recompiler la \texttt{netlist} avec un autre fichier.

\section{Conclusion}

Malgrés l'absence de spécification rigoureuse pour la sémantique d'une \texttt{netlist}, je pense que le simulateur proposé produit quelques chose d'assez cohérent avec ce qui était attendu.

Au rang des améliorations possibles, mais que je n'ai eu ni la motivation ni le temps de faire, on notera\footnote{de la moins nécessaire à la plus importante}
\begin{itemize}
\item la programmation d'un petit script exécutant toutes les netlist du dossier \texttt{test} afin de s'assurer qu'on obtient bien la sortie attendue.
\item étendre le compilateur aux entiers de plus de 64 bits
\item la programmation d'une petite interface graphique qui affiche les portes logiques et les cables électriques s'allumer en même temps qu'elle simule la netlist.
\end{itemize}

\end{document}
