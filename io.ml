open Format
open Netlist_ast

let romfile = ref ""
let compile = ref false
let keep    = ref false
let ofile   = ref "a.out"
let nb      = ref (-1)
let verbose = ref false

let fprintf_env fmt e =
  fprintf fmt "{";
  Env.iter
    (fun id v -> fprintf fmt "%s:%a@.;" id fprintf_value v)
    e;
  fprintf fmt "}@."

let print_mem mem =
  RMap.iter
    (fun (a, w) ram ->
      printf "addr size=%d; word size=%d@." a w;
      R.iter
        (fun addr data ->
          printf "- %a : %a@."
            fprintf_value addr
            fprintf_value data)
        ram)
    mem

(* Read stdin *)
let string_of_bit input =
  match int_of_string input with
  | 0 -> VBit false
  | 1 -> VBit true
  | _ -> error "Invalid binary value"

let string_of_bit_array n input =
  let bits = Array.make n false in

  if n <> String.length input
  then raise (Invalid_argument "Binary value is not of the right size");

  String.iteri
    (fun i c ->
      if c = '0'
      then bits.(n - i - 1) <- false
      else if c = '1'
      then bits.(n - i - 1) <- true
      else error "Invalid binary value")
    input;
  VBitArray bits

let rec read_input p_env senv input =
  try
    let value =
      match Env.find input p_env with
      | TBit | TBitArray 1 ->
         Printf.printf "%s (1) ? " input;
         string_of_bit (read_line ())
      | TBitArray n ->
         Printf.printf "%s (%d)? " input n;
         string_of_bit_array n (read_line ())
    in
    { senv with step = Env.add input value senv.step }
  with _ ->
    print_endline "Wrong input.";
    read_input p_env senv input

(* Some code for parsing rom files *)

(** Stack chars from chan in a buffer until
    a char from cs is reached *)
let lread_while cs chann =
  let buf = Buffer.create 10 in
  let rec aux () =
    let k = input_char chann in
    if not (List.mem k cs)
    then begin
        Buffer.add_char buf k;
        aux ();
      end
  in
  aux ();
  Buffer.contents buf

let read_while c chann = lread_while [c] chann

(** Parse a rom file open by chann
    The format for romfile is as follow:

    ```
    rom <addr_size> <word_size>
    <addr_1>=<value_1>
    ...
    ;
    ``` *)

let parse_rom_file chann =
  let rec parse_rom_value ((a, w) as r) roms rom =
    try

      let addr = lread_while ['='; ';'] chann in
      if addr = ""
      then parse_rom (RMap.add r rom roms)
      else
        let ident =
          try string_of_bit_array a addr
          with _ -> rom_error (addr ^ " is not a valid address.")
        in
        let v =
          let value = read_while '\n' chann in
          try string_of_bit_array w value
          with _ -> rom_error (value ^ " is not a valid value.")
        in
        parse_rom_value r roms (R.add ident v rom)
    with End_of_file -> RMap.add r rom roms

  and parse_rom roms =
    try
      let input = lread_while [' '; '\n'] chann in

      if input = ""
      then parse_rom roms
      else begin
          if input <> "rom"
          then rom_error "New rom have to start with rom.";

          let r =
            try
              let a = read_while ' ' chann in
              let b = read_while '\n' chann in
              int_of_string a, int_of_string b
            with _ ->
              rom_error "This is not a valid rom addr_size or word_size."
          in
          try parse_rom_value r roms (RMap.find r roms)
          with Not_found -> parse_rom_value r roms R.empty
        end
    with End_of_file -> roms
  in
  parse_rom RMap.empty
