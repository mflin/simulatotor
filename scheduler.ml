open Netlist_ast
open Graph

exception Combinational_cycle

let read_exp expr =
  let read_arg arg acc =
    match arg with
    | Avar ident -> ident :: acc
    | Aconst _ -> acc
  in
  let aux acc expr =
    match expr with
    | Earg (Avar ident) | Enot (Avar ident) -> ident :: acc
    | Ebinop (_, a, b) -> acc |> read_arg a |> read_arg b
    | Emux (a, b, c) -> acc |> read_arg a |> read_arg b |> read_arg c
    | Erom (_, _, a) -> acc |> read_arg a
    | Eram (_, _, read_addr, _, _, _) -> acc |> read_arg read_addr
    | Econcat (a, b) -> acc |> read_arg a |> read_arg b
    | Eslice (_, _, a) -> acc |> read_arg a
    | Eselect (_, a) -> acc |> read_arg a
    | _ -> acc
  in
  aux [] expr


let schedule p =
  let g = mk_graph () in
  Env.iter (fun id _ -> add_node g id) p.p_vars;
  List.iter (fun (id, exp) ->
      List.iter (fun x -> add_edge g x id) (read_exp exp))
    p.p_eqs;
  if has_cycle g
  then raise Combinational_cycle
  else
    let eqs =
      List.fold_left
        (fun acc x ->
          match List.assoc_opt x p.p_eqs with
          | Some eq -> (x, eq) :: acc
          | None -> acc)
        [] (topological g)
      |> List.rev
    in
    { p with p_eqs = eqs }
