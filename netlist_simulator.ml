open Netlist_ast
open Format
open Io

let null = function
  | TBit -> VBit false
  | TBitArray n -> VBitArray (Array.make n false)

(* Env where all variable is zero *)
let empty_env prog =
  let roms =
    if !romfile = ""
    then RMap.empty
    else
      let chann = open_in !romfile in
      let roms = parse_rom_file chann in
      close_in chann;
      roms
  in

  let rams =
    List.fold_left
      (fun rams (_, eq) ->
        match eq with
        | Eram (a, w, _, _, _, _) -> RMap.add (a, w) R.empty rams
        | _ -> rams)
      RMap.empty prog.p_eqs
  in
  {
    step =
      Env.fold
        (fun v t env -> Env.add v (null t) env)
        prog.p_vars Env.empty;
    registers = [];
    rams = rams;
    roms = roms;
    writes = [];
  }

let write_ram env next_env w =
  let r = (w.addr_size, w.word_size) in
  let ram = RMap.find r next_env.rams in
  let g = function
    | Avar ident -> Env.find ident env.step
    | Aconst v -> v
  in
  match g w.write_enable with
  | VBit true ->
    let ram = R.add (g w.write_addr) (g w.write_data) ram in
    { next_env with rams = RMap.add r ram env.rams }
  | VBit false -> next_env
  | _ -> error "Write enable should be only a 1 value."

(* Simulating netlist *)

let bnot = function
  | VBit v -> VBit (not v)
  | VBitArray v -> VBitArray (Array.map not v)

let op_of_binop = function
  | Or -> (||)
  | And -> (&&)
  | Xor -> fun a b -> (a && not b) || (not a && b)
  | Nand -> fun a b -> not (a && b)

let bbinop op a b =
  match a, b with
  | VBit x, VBit y -> VBit (op x y)
  | VBitArray x, VBitArray y ->
     VBitArray (Array.map2 op x y)
  | _ -> error "Invalid binop."

let value_of_exp senv exp =
  let get = function
    | Avar ident -> Env.find ident senv.step
    | Aconst v -> v
  in
  match exp with
  | Earg a -> get a
  | Enot a -> bnot (get a)
  | Ebinop (op, a, b) ->
     bbinop (op_of_binop op) (get a) (get b)
  | Emux (a, b, c) ->
     begin match get a with
     | VBit true -> get c
     | VBit false -> get b
     | _ -> error "Invalid multiplexer."
     end
  | Econcat (a, b) ->
     begin match get a, get b with
     | VBit a, VBit b -> VBitArray [|a; b|]
     | VBitArray a, VBit b -> VBitArray (Array.append a [|b|])
     | VBit a, VBitArray b -> VBitArray (Array.append [|a|] b)
     | VBitArray a, VBitArray b -> VBitArray (Array.append a b)
     end
  | Eslice (i, j, a) ->
     begin match get a with
     | VBitArray a -> VBitArray (Array.sub a i (j - i + 1))
     | VBit _ -> error "Cannot get slice of single bit."
     end
  | Eselect (i, a) ->
     begin match get a with
     | VBit _ as a ->
        if i = 0
        then a
        else error "Cannot take ith bit of a"
     | VBitArray a -> VBit a.(i)
     end
  | _ -> assert false

let simulate_eq senv (id, exp) =
  let get = function
    | Avar ident -> Env.find ident senv.step
    | Aconst v -> v
  in
  match exp with
  | Ereg ident ->
     (* remember to remember the value at end of step *)
     { senv with registers = (id, ident) :: senv.registers }
  | Eram (addr_size, word_size, read_addr,
          write_enable, write_addr, write_data) ->
     let r = (addr_size, word_size) in
     let read = get read_addr in
     let ram = RMap.find r senv.rams in

     (* read value in ram *)
     let rams, step =
       try
         senv.rams, Env.add id (R.find read ram) senv.step
       with Not_found ->
         let null = VBitArray (Array.make word_size false) in
         let ram = R.add read null ram in
         RMap.add r ram senv.rams, Env.add id null senv.step
     in

     let writes =
       {
         write_enable = write_enable;
         addr_size = addr_size;
         word_size = word_size;
         write_addr = write_addr;
         write_data = write_data
       } :: senv.writes
     in

     { senv with step = step; rams = rams; writes = writes }
  | Erom (addr_size, word_size, read_addr) ->
     begin try
         let rom = RMap.find (addr_size, word_size) senv.roms in
         { senv with
           step = Env.add id (R.find (get read_addr) rom) senv.step}
       with Not_found -> error "Unknow given address in rom."
     end
  | _ -> { senv with step = Env.add id (value_of_exp senv exp) senv.step }

let simulate prog =
  let empty = empty_env prog in
  let rec step i senv =
    if !nb >= 0 && i > !nb then begin
        printf "All steps done.@.";
        exit 0;
      end;

    printf "Step %d:@." i;
    if !verbose then begin
        printf "Ram:@.";
        print_mem senv.rams;
        printf "Rom:@.";
        print_mem senv.roms;
      end;

    (* Execute this step *)
    let env =
      List.fold_left simulate_eq
        (List.fold_left
           (read_input prog.p_vars)
           senv prog.p_inputs)
        prog.p_eqs
    in

    (* Print values of output vars *)
    List.iter
      (fun output ->
        printf "=> %s = %a@." output
          fprintf_value (Env.find output env.step))
      prog.p_outputs;

    (* Save registers *)
    let next_env =
      List.fold_left
        (fun next_env (id, exp) ->
          { next_env with
            step = Env.add id (Env.find exp env.step) next_env.step })
        { empty with rams = env.rams; roms = env.roms }
        env.registers
    in

    (* Write on ram new data *)
    let next_env = List.fold_left (write_ram env) next_env env.writes in

    (* Start next step *)
    step (i+1) next_env
  in
  step 1 empty
