build:
	dune build main.exe

clean:
	dune clean

re: clean build
