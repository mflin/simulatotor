open Format
open Netlist_ast
open Netlist_printer

exception Compile_error of string
let compile_error msg = raise (Compile_error msg)

let const = Hashtbl.create 64

let sizeof v env =
  match Env.find v env with
  | TBit -> 1
  | TBitArray n -> n

let vsizeof = function
  | VBit _ -> 1
  | VBitArray bits -> Array.length bits

let value_to_int = function
  | VBit false -> 0
  | VBit true -> 1
  | VBitArray a ->
     Array.fold_right
       (fun x acc -> if x then 1 + 2 * acc else 2 * acc)
       a 0

let fprintf_type fmt = function
  | TBit                    -> fprintf fmt "unsigned char"
  | TBitArray n when n <= 8  -> fprintf fmt "unsigned char"
  | TBitArray n when n <= 16 -> fprintf fmt "unsigned short"
  | TBitArray n when n <= 32 -> fprintf fmt "unsigned int"
  | TBitArray n when n <= 64 -> fprintf fmt "unsigned long"
  | _ -> compile_error "Can't handle more than 64 bits"

let int_to_bits a x =
  let x = ref x in
  let m = Array.make a false in
  for i = 0 to a - 1 do
    m.(i) <- !x mod 2 <> 0;
    x := !x / 2
  done;
  VBitArray m

let save_const = function
  | Avar _ -> ()
  | Aconst ((VBitArray a) as v) ->
     let v = value_to_int v in
     Hashtbl.replace const (sprintf "__const%x" v) (Array.length a, v)
  | Aconst (VBit true) ->
     Hashtbl.replace const "__const1" (1, 1)
  | Aconst (VBit false) ->
     Hashtbl.replace const "__const0" (1, 0)

let decl_var fmt prog =
  Env.iter
    (fun v ty ->
      fprintf fmt "%a %s = 0;\n" fprintf_type ty v)
    prog.p_vars;

  let rams =
    List.fold_left
      (fun rams (_, eq) ->
        match eq with
        | Eram (a, w, _, we, wa, wd) ->
           List.iter save_const [we; wa; wd];
           if List.mem (a, w) rams
           then rams else (a, w) :: rams
        | _ -> rams)
      [] prog.p_eqs
  in

  Hashtbl.iter
    (fun ident (s, v) ->
      fprintf fmt "%a %s = 0x%x;@."
        fprintf_type (TBitArray s)
        ident v)
    const;

  List.iteri
    (fun i (a, w) ->
      fprintf fmt "%a __ram_%d_%d[1 << %d] = { 0 };\n"
        fprintf_type (TBitArray w)
        a w a;
      fprintf fmt "unsigned char *__ram_%d_%d_use_%d_we = 0;\n" a w i;
      fprintf fmt "%a *__ram_%d_%d_use_%d_wa = 0;\n"
        fprintf_type (TBitArray a) a w i;
      fprintf fmt "%a *__ram_%d_%d_use_%d_wd = 0;\n"
        fprintf_type (TBitArray w) a w i)
    rams;

    let roms =
    if !Io.romfile = ""
    then RMap.empty
    else
      let chann = open_in !Io.romfile in
      let roms = Io.parse_rom_file chann in
      close_in chann;
      roms
  in

  RMap.iter
    (fun (a, w) rom ->
      fprintf fmt "%a __rom_%d_%d[1 << %d] = {"
        fprintf_type (TBitArray w) a w a;
      let n = 1 lsl a in
      for i = 0 to n - 1 do
        begin try let v = R.find (int_to_bits a i) rom in
                  fprintf fmt "0b%a" fprintf_value v;
              with Not_found -> fprintf fmt "0x0" end ;
        if i < n - 1 then fprintf fmt ",";
      done;
      fprintf fmt "};\n")
    roms

let set_vars fmt prog =
  let registers =
    List.fold_left
      (fun rs (v, eq) ->
        match eq with
        | Ereg id -> fprintf fmt "%s = %s;\n" v id; v :: rs
        | _ -> rs)
      [] prog.p_eqs
  in
  Env.iter
    (fun v _ ->
      if not (List.mem v registers)
      then fprintf fmt "%s = 0;\n" v)
    prog.p_vars

let read_inputs fmt prog =
  List.iter
    (fun i ->
      let ty = Env.find i prog.p_vars in
      let t = match ty with
          TBit -> "1"
        | TBitArray n -> string_of_int n
      in
      fprintf fmt "printf(\"%s (%s)? \"); fflush(stdout); __rc = scanf(\"%%x\", &__tmp); %s = (%a)__tmp;
if (__rc == EOF) { exit(0); }
if (__rc == 0) { while(fgetc(stdin) != '\\n'); }\n" i t i fprintf_type ty)
    prog.p_inputs

let print_outputs fmt prog =
  List.iter
    (fun o -> fprintf fmt "printf(\"=> %s : 0x%%x\\n\", %s);\n" o o)
    prog.p_outputs

let fprintf_arg fmt = function
  | Avar id -> fprintf fmt "%s" id
  | Aconst v -> fprintf fmt "%x" (value_to_int v)

let fprintf_binop fmt = function
  | Or -> fprintf fmt "|"
  | Xor -> fprintf fmt "^"
  | And -> fprintf fmt "&"
  | Nand -> assert false

let compile_eq prog fmt = function
  | Earg a -> fprintf fmt "%a" fprintf_arg a
  | Enot a -> fprintf fmt "!%a" fprintf_arg a
  | Ebinop (Nand, a1, a2) ->
     fprintf fmt "~(%a & %a)"
       fprintf_arg a1
       fprintf_arg a2
  | Ebinop (op, a1, a2) ->
     fprintf fmt "(%a %a %a)"
       fprintf_arg a1
       fprintf_binop op
       fprintf_arg a2
  | Econcat (Avar a1, a2) ->
     fprintf fmt "(%a << 0x%x) ^ %s"
       fprintf_arg a2 (sizeof a1 prog.p_vars) a1
  | Econcat (Aconst v, a2) ->
     fprintf fmt "(%a << 0x%x) ^ %x"
       fprintf_arg a2 (vsizeof v) (value_to_int v)
  | Eselect (i, a) ->
     fprintf fmt "((%a >> 0x%x) & 1)" fprintf_arg a i
  | Eslice (i1, i2, a) ->
     fprintf fmt "(%a >> 0x%x) & ((1L << (0x%x - 0x%x + 1)) - 1)"
       fprintf_arg a i1 i2 i1
  | Emux (a, b, c) ->
     fprintf fmt "(%a ? %a : %a)"
       fprintf_arg a
       fprintf_arg c
       fprintf_arg b
  | Ereg _ | Erom _ | Eram _ -> assert false

let compile_eqs fmt prog =
  let fprintf_ref fmt = function
    | Aconst v -> fprintf fmt "&__const%x" (value_to_int v)
    | Avar ident -> fprintf fmt "&%s" ident
  in
  let ramc = ref 0 in
  List.iter
    (fun ((x, eq) as e) ->
      fprintf fmt "\n// %a" print_eq e;
      match eq with
      | Ereg _ -> ()
      | Erom (a, w, ra) ->
         fprintf fmt "%s = __rom_%d_%d[%a];\n" x a w fprintf_arg ra
      | Eram (a, w, ra, we, wa, wd) ->
         fprintf fmt "%s = __ram_%d_%d[%a];\n" x a w fprintf_arg ra;
         fprintf fmt "__ram_%d_%d_use_%d_we = %a;
__ram_%d_%d_use_%d_wa = %a;
__ram_%d_%d_use_%d_wd = %a;\n"
           a w !ramc fprintf_ref we
           a w !ramc fprintf_ref wa
           a w !ramc fprintf_ref wd;
         incr ramc
      | _ -> fprintf fmt "%s = %a;\n" x (compile_eq prog) eq)
    prog.p_eqs;

  ramc := 0;
  List.iter
    (fun (_, eq) ->
      match eq with
      | Eram (a, w, _, _, _, _) ->
         fprintf fmt "if (*__ram_%d_%d_use_%d_we) {
__ram_%d_%d[*__ram_%d_%d_use_%d_wa] = *__ram_%d_%d_use_%d_wd;
}\n" a w !ramc a w a w !ramc a w !ramc;
         incr ramc
      | _ -> ())
    prog.p_eqs

let code fmt prog =
  fprintf fmt
"// C file generated from netlist
#include <stdio.h>
#include <stdlib.h>

unsigned long __tmp = 0;

// declare variables
%a
int main(void) {
int __rc;
for (unsigned long __i = 1; __i <= %d; __i++) {
printf(\"Step %%d:\\n\", __i);
// set vars values
%a
//read inputs
%a
// make step
%a
// print outputs
%a
}
return 0;
}"
decl_var prog
!Io.nb
set_vars prog
read_inputs prog
compile_eqs prog
print_outputs prog

let compile prog =
  let tmp = Filename.temp_file "simulatotor" ".c" in
  let out = open_out tmp in
  let fmt = formatter_of_out_channel out in

  fprintf fmt "%a@?" code prog;

  let _ = Sys.command (sprintf "gcc -g -O3 %s -o %s" tmp !Io.ofile) in

  if !Io.keep
  then ignore(Sys.command (sprintf "cp %s %s.c" tmp !Io.ofile));

  let _ = Sys.command (sprintf "rm %s" tmp) in
  ()
